--changeset bondareva:2023-05-04T16:45:00
CREATE TABLE public.person (
    person_id UUID DEFAULT gen_random_uuid() NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    crypto_password VARCHAR(255) NOT NULL,
    CONSTRAINT "PERSON_pkey" PRIMARY KEY (person_id)
);
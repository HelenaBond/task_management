--changeset bondareva:2023-05-04T17:30:00
CREATE TABLE public.comment (
    comment_id UUID DEFAULT gen_random_uuid() NOT NULL,
    author UUID NOT NULL,
    task UUID NOT NULL,
    message VARCHAR(2048) NOT NULL,
    date_of_creating_utc TIMESTAMP(0) NOT NULL,
    CONSTRAINT "COMMENT_pkey" PRIMARY KEY (comment_id),
    CONSTRAINT "fk_author_id" FOREIGN KEY (author) REFERENCES public.person(person_id),
    CONSTRAINT "fk_task_id" FOREIGN KEY (task) REFERENCES public.task(task_id)
);
--changeset bondareva:2023-05-04T17:00:00
CREATE TABLE public.task (
    task_id UUID DEFAULT gen_random_uuid() NOT NULL,
    title VARCHAR(255) NOT NULL,
    description VARCHAR(2048) NOT NULL,
    status VARCHAR(20) NOT NULL,
    priority VARCHAR(20) NOT NULL,
    performer UUID NOT NULL,
    author UUID NOT NULL,
    CONSTRAINT "TASK_pkey" PRIMARY KEY (task_id),
    CONSTRAINT "fk_performer_id" FOREIGN KEY (performer) REFERENCES public.person(person_id),
    CONSTRAINT "fk_author_id" FOREIGN KEY (author) REFERENCES public.person(person_id)
);
package com.bondareva.task_management.rest.enums;

public enum TaskStatus {
    NEW, PROGRESS, COMPLETE
}

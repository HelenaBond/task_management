package com.bondareva.task_management.rest.enums;

public enum TaskPriority {
    LOW, MEDIUM, HIGH
}

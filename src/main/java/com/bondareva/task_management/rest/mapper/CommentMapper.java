package com.bondareva.task_management.rest.mapper;

import com.bondareva.task_management.rest.dto.comment.response.CommentDto;
import com.bondareva.task_management.rest.model.Comment;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        injectionStrategy = InjectionStrategy.CONSTRUCTOR,
        unmappedSourcePolicy = ReportingPolicy.WARN)
public interface CommentMapper {

    @Mapping(target = "authorEmail", source = "authorEmail")
    CommentDto fromCommentToDto(Comment comment, String authorEmail);
}

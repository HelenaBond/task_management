package com.bondareva.task_management.rest.mapper;

import com.bondareva.task_management.auth.model.Person;
import com.bondareva.task_management.rest.dto.task.request.CreateTask;
import com.bondareva.task_management.rest.dto.task.request.UpdateTask;
import com.bondareva.task_management.rest.dto.task.response.TaskDto;
import com.bondareva.task_management.rest.dto.task.request.FilterTask;
import com.bondareva.task_management.rest.enums.TaskStatus;
import com.bondareva.task_management.rest.model.Task;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.UUID;

@Mapper(componentModel = "spring",
        injectionStrategy = InjectionStrategy.CONSTRUCTOR,
        unmappedSourcePolicy = ReportingPolicy.WARN)
public interface TaskMapper {

    @Mapping(target = "taskId", ignore = true)
    @Mapping(target = "status", source = "status")
    @Mapping(target = "author", source = "author")
    @Mapping(target = "performer", source = "performer")
    Task fromCreateDtoToTask(CreateTask dto, TaskStatus status, Person author, Person performer);

    @Mapping(target = "authorEmail", source = "authorEmail")
    @Mapping(target = "performerEmail", source = "performerEmail")
    TaskDto fromTaskToTaskDto(Task task, String authorEmail, String performerEmail);

    @Mapping(target = "taskId", source = "taskId")
    @Mapping(target = "author", source = "author")
    @Mapping(target = "performer", source = "performer")
    Task fromUpdateDtoToTask(UUID taskId, UpdateTask task, Person author, Person performer);

    @Mapping(target = "taskId", ignore = true)
    @Mapping(target = "author", source = "author")
    @Mapping(target = "performer", source = "performer")
    Task fromFilterToTask(FilterTask dto, Person performer, Person author);
}

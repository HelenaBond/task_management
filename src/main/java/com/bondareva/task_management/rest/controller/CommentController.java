package com.bondareva.task_management.rest.controller;

import com.bondareva.task_management.exception.ErrorMessage;
import com.bondareva.task_management.rest.dto.comment.request.CommentGetAllRequest;
import com.bondareva.task_management.rest.dto.comment.request.CreateComment;
import com.bondareva.task_management.rest.dto.comment.request.UpdateComment;
import com.bondareva.task_management.rest.dto.comment.response.CommentDto;
import com.bondareva.task_management.rest.dto.comment.response.CommentGetAllResponse;
import com.bondareva.task_management.rest.service.CommentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Tag(name = "Управление коментариями.")
@ApiResponses(value = {
        @ApiResponse(responseCode = "500", description = "Ошибка сервера. Обратитесь в техподдержку.",
                content = {@Content(mediaType = "application/json",
                        schema = @Schema(implementation = ErrorMessage.class))}),
        @ApiResponse(
                responseCode = "400",
                description = """
                        Возможные варианты ответа:
                         1. Неверный формат данных запроса или отсутствуют обязательные поля.
                         2. Невалидный токен.""",
                content = {@Content(mediaType = "application/json",
                        schema = @Schema(implementation = ErrorMessage.class))}),
        @ApiResponse(
                responseCode = "401",
                description = "Время жизни токена истекло. Пожалуйста авторизуйтесь.",
                content = {@Content(mediaType = "application/json",
                        schema = @Schema(implementation = ErrorMessage.class))})

})
@RestController
@RequestMapping("/comment")
@RequiredArgsConstructor
public class CommentController {

    private final CommentService commentService;

    @Operation(summary = "Создать новый комментарий.")
    @ApiResponse(
            responseCode = "200",
            description = """
                             Новый комментарий создан успешно.
                              Возвращается сохранённый комментарий с временем создания
                              в формате yyyy-mm-ddThh:mm:ss UTC.""",
            content = @Content(mediaType = "application/json"))

    @ApiResponse(
            responseCode = "400",
            description = "Вы не можете оставить коментарий к выбранной задаче. Задача не найдена.",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorMessage.class))})

    @PostMapping("/{taskId}")
    public CommentDto add(@PathVariable("taskId") UUID taskId,
                          @ParameterObject @RequestBody @Validated CreateComment createComment) {
        return commentService.add(taskId, createComment);
    }

    @Operation(summary = "Обновить комментарий.")
    @ApiResponse(
            responseCode = "200",
            description = "Комментарий обновлен успешно. Возвращается обновлённый коментарий.",
            content = @Content(mediaType = "application/json"))

    @ApiResponse(
            responseCode = "400",
            description = """
                        Возможные варианты ответа:
                         1. Комментарий не найден.
                         2. Вы не можете изменить комментарий. Вы не автор этого комментария.""",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorMessage.class))})

    @PatchMapping("/{commentId}")
    public CommentDto update(
            @PathVariable("commentId") UUID commentId,
            @ParameterObject @RequestBody @Validated UpdateComment updateComment) {
        return commentService.update(commentId, updateComment);
    }

    @Operation(summary = "Удалить комментарий.")
    @ApiResponse(
            responseCode = "200",
            description = "Комментарий удален успешно.",
            content = @Content(mediaType = "application/json"))

    @ApiResponse(
            responseCode = "400",
            description = """
                        Возможные варианты ответа:
                         1. Комментарий не найден.
                         2. Вы не можете удалить комментарий. Вы не автор этого комментария.""",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorMessage.class))})

    @DeleteMapping("/{commentId}")
    public ResponseEntity<Void> delete(
            @Parameter(name = "id", description = "Идентификатор комментария.", required = true)
            @PathVariable("commentId") UUID commentId) {
        commentService.delete(commentId);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Получить ограниченный в размере список комментариев к задаче.")
    @ApiResponse(
            responseCode = "200",
            description = "Возвращает упорядоченный по дате добавления (сначала старые) список комментариев.",
            content = @Content(mediaType = "application/json"))

    @GetMapping("/all/{taskId}")
    public CommentGetAllResponse getAll(
            @Parameter(name = "id", description = "Идентификатор задачи.", required = true)
            @PathVariable("taskId") UUID taskId,
            @ParameterObject @RequestBody @Validated CommentGetAllRequest commentGetAllRequest) {
        return commentService.getAll(taskId, commentGetAllRequest);
    }
}

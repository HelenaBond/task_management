package com.bondareva.task_management.rest.controller;

import com.bondareva.task_management.exception.ErrorMessage;
import com.bondareva.task_management.rest.dto.task.request.CreateTask;
import com.bondareva.task_management.rest.dto.task.request.TaskGetAllRequest;
import com.bondareva.task_management.rest.dto.task.request.UpdateTask;
import com.bondareva.task_management.rest.dto.task.request.UpdateTaskStatus;
import com.bondareva.task_management.rest.dto.task.response.TaskDto;
import com.bondareva.task_management.rest.dto.task.response.TaskGetAllResponse;
import com.bondareva.task_management.rest.service.TaskService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Tag(name = "Управление задачами.")
@ApiResponses(value = {
        @ApiResponse(responseCode = "500", description = "Ошибка сервера. Обратитесь в техподдержку.",
                content = {@Content(mediaType = "application/json",
                        schema = @Schema(implementation = ErrorMessage.class))}),
        @ApiResponse(
                responseCode = "400",
                description = """
                        Возможные варианты ответа:
                         1. Неверный формат данных запроса или отсутствуют обязательные поля.
                         2. Невалидный токен.""",
                content = {@Content(mediaType = "application/json",
                        schema = @Schema(implementation = ErrorMessage.class))}),
        @ApiResponse(
                responseCode = "401",
                description = "Время жизни токена истекло. Пожалуйста авторизуйтесь.",
                content = {@Content(mediaType = "application/json",
                        schema = @Schema(implementation = ErrorMessage.class))})

})
@RestController
@RequestMapping("/task")
@RequiredArgsConstructor
public class TaskController {

    private final TaskService taskService;

    @Operation(summary = "Создать новую задачу.")
    @ApiResponse(
            responseCode = "200",
            description = "Новая задача создана успешно. Возвращается сохранённая задача со статусом NEW и почтой автора.",
            content = @Content(mediaType = "application/json"))

    @ApiResponse(
            responseCode = "400",
            description = """
                        Возможные варианты ответа:
                         1. Назначаемый исполнитель не найден.
                         2. Не получилось сохранить задачу без исполнителя. Сообщите пожалуйста об этом в техподдержку.""",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorMessage.class))})

    @PostMapping()
    public TaskDto add(@ParameterObject @RequestBody @Validated CreateTask createTask) {
        return taskService.add(createTask);
    }

    @Operation(summary = "Обновить задачу.")
    @ApiResponse(
            responseCode = "200",
            description = "Задача обновлена успешно. Возвращается обновлённая задача.",
            content = @Content(mediaType = "application/json"))

    @ApiResponse(
            responseCode = "400",
            description = """
                        Возможные варианты ответа:
                         1. Задача не найдена.
                         2. Вы не можете изменить эту задачу. Вы не автор выбранной задачи.
                         3. Назначаемый исполнитель не найден.
                         4. Не получилось сохранить задачу без исполнителя. Сообщите пожалуйста об этом в техподдержку.""",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorMessage.class))})

    @PutMapping("/{id}")
    public TaskDto update(
            @Parameter(name = "id", description = "Идентификатор задачи.", required = true)
            @PathVariable("id") UUID taskId,
            @ParameterObject @RequestBody @Validated UpdateTask updateTask) {
        return taskService.update(taskId, updateTask);
    }

    @Operation(summary = "Обновить статус задачи.")
    @ApiResponse(
            responseCode = "200",
            description = "Статус задачи обновлен успешно.",
            content = @Content(mediaType = "application/json"))

    @ApiResponse(
            responseCode = "400",
            description = """
                        Возможные варианты ответа:
                         1. Задача не найдена.
                         2. Вы не можете изменить статус этой задачи. Вы не исполнитель и не автор выбранной задачи.""",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorMessage.class))})

    @PatchMapping("/{id}/status")
    public ResponseEntity<Void> updateStatus(
            @Parameter(name = "id", description = "Идентификатор задачи.", required = true)
            @PathVariable("id") UUID taskId,
            @ParameterObject @RequestBody @Validated UpdateTaskStatus updateStatus) {
        taskService.updateStatus(taskId, updateStatus);
        return ResponseEntity.accepted().build();
    }

    @Operation(summary = "Удалить задачу.")
    @ApiResponse(
            responseCode = "200",
            description = "Задача удалена успешно.",
            content = @Content(mediaType = "application/json"))

    @ApiResponse(
            responseCode = "400",
            description = """
                        Возможные варианты ответа:
                         1. Задача не найдена.
                         2. Вы не можете удалить эту задачу. Вы не автор выбранной задачи.""",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorMessage.class))})

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(
            @Parameter(name = "id", description = "Идентификатор задачи.", required = true)
            @PathVariable("id") UUID taskId) {
        taskService.delete(taskId);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Получить ограниченный в размере список задач.")
    @ApiResponse(
            responseCode = "200",
            description = "Возвращает не упорядоченный отсортированный список задач.",
            content = @Content(mediaType = "application/json"))

    @GetMapping("/all")
    public TaskGetAllResponse all(@ParameterObject @RequestBody @Validated TaskGetAllRequest getAllRequest) {
        return taskService.findAll(getAllRequest);
    }
}

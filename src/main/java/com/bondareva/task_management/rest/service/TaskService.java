package com.bondareva.task_management.rest.service;

import com.bondareva.task_management.auth.dto.PersonDto;
import com.bondareva.task_management.auth.model.Person;
import com.bondareva.task_management.auth.repo.PersonRepo;
import com.bondareva.task_management.exception.ValidationException;
import com.bondareva.task_management.rest.dto.PageResponseDto;
import com.bondareva.task_management.rest.dto.task.request.CreateTask;
import com.bondareva.task_management.rest.dto.task.request.UpdateTask;
import com.bondareva.task_management.rest.dto.task.response.TaskDto;
import com.bondareva.task_management.rest.dto.task.request.TaskGetAllRequest;
import com.bondareva.task_management.rest.dto.task.request.UpdateTaskStatus;
import com.bondareva.task_management.rest.dto.task.response.TaskGetAllResponse;
import com.bondareva.task_management.rest.enums.TaskStatus;
import com.bondareva.task_management.rest.mapper.TaskMapper;
import com.bondareva.task_management.rest.model.Task;
import com.bondareva.task_management.rest.repo.TaskRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final CurrentUser currentUser;
    private final TaskMapper taskMapper;
    private final TaskRepo taskRepo;
    private final PersonRepo personRepo;
    private final CommentService commentService;

    public TaskDto add(CreateTask dto) {
        Person author = currentUser.getPersonFromHolder();
        Person performer = validateOrDefaultPerson(author, dto.performerEmail());
        Task task = taskMapper.fromCreateDtoToTask(dto, TaskStatus.NEW, author, performer);
        Task savedTask = taskRepo.save(task);
        return taskMapper.fromTaskToTaskDto(savedTask, author.getEmail(), performer.getEmail());
    }

    public TaskDto update(UUID taskId, UpdateTask dto) {
        Task findTask = taskRepo.findById(taskId).orElseThrow(
                () -> new ValidationException("Задача не найдена."));
        Person author = currentUser.getPersonFromHolder();
        if (!author.getPersonId().equals(findTask.getAuthor().getPersonId())) {
            throw new ValidationException("Вы не можете изменить эту задачу. Вы не автор выбранной задачи.");
        }
        Person performer = validateOrDefaultPerson(author, dto.performerEmail());
        Task updatedTask = taskMapper.fromUpdateDtoToTask(taskId, dto, author, performer);
        Task savedTask = taskRepo.save(updatedTask);
        return taskMapper.fromTaskToTaskDto(savedTask, author.getEmail(), performer.getEmail());
    }

    private Person validateOrDefaultPerson(Person author, String performerEmail) {
        Person performer;
        if (performerEmail.equals("default")) {
            performer = personRepo.findByEmail("default").orElseThrow(
                    () -> new ValidationException(
                            "Пользователь по умолчанию не найден. Сообщите пожалуйста об этом в техподдержку."));
        } else {
            if (performerEmail.equals(author.getEmail())) {
                performer = author;
            } else {
                PersonDto personView = personRepo.findPersonDtoByEmail(performerEmail).orElseThrow(
                        () -> new ValidationException("Назначаемый исполнитель не найден."));
                performer = Person.builder().personId(personView.personId()).email(personView.email()).build();
            }
        }
        return performer;
    }

    public void updateStatus(UUID taskId, UpdateTaskStatus dto) {
        Task findTask = taskRepo.findById(taskId).orElseThrow(
                () -> new ValidationException("Задача не найдена.")
        );
        Person user = currentUser.getPersonFromHolder();
        if (!findTask.getPerformer().getPersonId().equals(user.getPersonId())
                && !findTask.getAuthor().getPersonId().equals(user.getPersonId())) {
            throw new ValidationException(
                    "Вы не можете изменить статус этой задачи. Вы не исполнитель и не автор выбранной задачи.");
        }

        if (!findTask.getStatus().equals(dto.status())) {
            findTask.setStatus(dto.status());
            taskRepo.save(findTask);
        }
    }

    public void delete(UUID taskId) {
        Task findTask = taskRepo.findById(taskId).orElseThrow(() -> new ValidationException("Задача не найдена."));
        Person author = currentUser.getPersonFromHolder();
        if (!findTask.getAuthor().getPersonId().equals(author.getPersonId())) {
            throw new ValidationException("Вы не можете удалить эту задачу. Вы не автор выбранной задачи.");
        }
        commentService.deleteAll(taskId);
        taskRepo.delete(findTask);
    }

    public TaskGetAllResponse findAll(TaskGetAllRequest dto) {
        Person performer = Person.builder()
                .email(dto.filterDto().performerEmail())
                .build();
        Person author = Person.builder()
                .email(dto.filterDto().authorEmail())
                .build();
        Task filter = taskMapper.fromFilterToTask(dto.filterDto(), performer, author);
        Pageable pageable = PageRequest.of(dto.pageRequestDto().currentPage(), dto.pageRequestDto().pageSize());
        Page<Task> listTask = filterAllTask(filter, pageable);
        List<UUID> uuids = listTask.getContent().stream().map(Task::getTaskId).toList();
        List<TaskDto> tasks = taskRepo.findAllByTaskIdIn(uuids);
        PageResponseDto pageResponseDto = new PageResponseDto(listTask.getTotalPages(), listTask.getNumber());
        return new TaskGetAllResponse(tasks, pageResponseDto);
    }

    private Page<Task> filterAllTask(Task filter, Pageable pageable) {
        ExampleMatcher matcher = ExampleMatcher.matchingAll()
                .withMatcher("title", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
                .withMatcher("description", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
                .withMatcher("performer.email", ExampleMatcher.GenericPropertyMatchers.exact())
                .withMatcher("author.email", ExampleMatcher.GenericPropertyMatchers.exact());
        Example<Task> example = Example.of(filter, matcher);
        return taskRepo.findAll(example, pageable);
    }
}

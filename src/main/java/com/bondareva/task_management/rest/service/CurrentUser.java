package com.bondareva.task_management.rest.service;

import com.bondareva.task_management.auth.dto.UserDetailsImpl;
import com.bondareva.task_management.auth.model.Person;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class CurrentUser {

    public Person getPersonFromHolder() {
        UserDetailsImpl personUserDetails = (UserDetailsImpl) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        return Person.builder()
                .email(personUserDetails.getUsername())
                .personId(personUserDetails.getId())
                .build();
    }
}

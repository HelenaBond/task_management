package com.bondareva.task_management.rest.service;

import com.bondareva.task_management.auth.model.Person;
import com.bondareva.task_management.exception.ValidationException;
import com.bondareva.task_management.rest.dto.PageRequestDto;
import com.bondareva.task_management.rest.dto.PageResponseDto;
import com.bondareva.task_management.rest.dto.comment.request.CommentGetAllRequest;
import com.bondareva.task_management.rest.dto.comment.request.CreateComment;
import com.bondareva.task_management.rest.dto.comment.request.UpdateComment;
import com.bondareva.task_management.rest.dto.comment.response.CommentDto;
import com.bondareva.task_management.rest.dto.comment.response.CommentGetAllResponse;
import com.bondareva.task_management.rest.mapper.CommentMapper;
import com.bondareva.task_management.rest.model.Comment;
import com.bondareva.task_management.rest.model.Task;
import com.bondareva.task_management.rest.repo.CommentRepo;
import com.bondareva.task_management.rest.repo.TaskRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CommentService {

    private final CommentRepo commentRepo;
    private final CurrentUser currentUser;
    private final TaskRepo taskRepo;
    private final CommentMapper commentMapper;

    public CommentDto add(UUID taskId, CreateComment dto) {
        Person author = currentUser.getPersonFromHolder();
        Task task = taskRepo.findById(taskId).orElseThrow(
                () -> new ValidationException("Вы не можете оставить коментарий к выбранной задаче. Задача не найдена."));
        Comment comment = Comment.builder()
                .author(author)
                .task(task)
                .message(dto.message())
                .dateOfCreatingUtc(LocalDateTime.now(ZoneOffset.UTC).withNano(0))
                .build();
        Comment savedComment = commentRepo.save(comment);
        return commentMapper.fromCommentToDto(savedComment, author.getEmail());
    }


    public CommentDto update(UUID commentId, UpdateComment dto) {
        Person author = currentUser.getPersonFromHolder();
        Comment comment = commentRepo.findById(commentId).orElseThrow(
                () -> new ValidationException("Комментарий не найден."));
        if (!author.getPersonId().equals(comment.getAuthor().getPersonId())) {
            throw new ValidationException("Вы не можете изменить комментарий. Вы не автор этого комментария.");
        }
        comment.setMessage(dto.message());
        Comment savedComment = commentRepo.save(comment);
        return commentMapper.fromCommentToDto(savedComment, author.getEmail());
    }

    public void delete(UUID commentId) {
        Person author = currentUser.getPersonFromHolder();
        Comment comment = commentRepo.findById(commentId).orElseThrow(
                () -> new ValidationException("Комментарий не найден."));
        if (!author.getPersonId().equals(comment.getAuthor().getPersonId())) {
            throw new ValidationException("Вы не можете удалить комментарий. Вы не автор этого комментария.");
        }
        commentRepo.delete(comment);
    }

    public CommentGetAllResponse getAll(UUID taskId, CommentGetAllRequest dto) {
        PageRequestDto pageRequestDto = dto.pageRequestDto();
        Pageable pageable = PageRequest.of(pageRequestDto.currentPage(), pageRequestDto.pageSize());
        if (!taskRepo.existsById(taskId)) {
            throw new ValidationException("Вы не можете посмотреть коментарии к выбранной задаче. Задача не найдена.");
        }
        Page<CommentDto> result = commentRepo.findAllByTask_TaskIdOrderByDateOfCreatingUtcAsc(taskId, pageable);
        PageResponseDto page = new PageResponseDto(result.getTotalPages(), result.getNumber());
        return new CommentGetAllResponse(result.getContent(), page);
    }

    public void deleteAll(UUID taskId) {
        commentRepo.deleteAllComments(taskId);
    }
}

package com.bondareva.task_management.rest.repo;

import com.bondareva.task_management.rest.dto.task.response.TaskDto;
import com.bondareva.task_management.rest.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface TaskRepo extends JpaRepository<Task, UUID> {

    List<TaskDto> findAllByTaskIdIn(List<UUID> taskIds);

    //for tests
    Task findByTitle(String title);
}
package com.bondareva.task_management.rest.repo;

import com.bondareva.task_management.rest.dto.comment.response.CommentDto;
import com.bondareva.task_management.rest.model.Comment;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;

public interface CommentRepo extends JpaRepository<Comment, UUID> {

    Page<CommentDto> findAllByTask_TaskIdOrderByDateOfCreatingUtcAsc(UUID taskId, Pageable pageable);

    @Transactional
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("DELETE FROM Comment c WHERE c.task.taskId = :id")
    void deleteAllComments(@Param("id") UUID taskId);

    //for tests
    Comment findByMessage(String message);
}

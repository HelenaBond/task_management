package com.bondareva.task_management.rest.model;

import com.bondareva.task_management.auth.model.Person;
import com.bondareva.task_management.rest.enums.TaskPriority;
import com.bondareva.task_management.rest.enums.TaskStatus;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.UuidGenerator;

import java.util.UUID;

import static jakarta.persistence.EnumType.STRING;
import static jakarta.persistence.FetchType.LAZY;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Task {

    @Id
    @GeneratedValue
    @UuidGenerator
    @Column(updatable = false)
    private UUID taskId;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    @Enumerated(STRING)
    private TaskStatus status;

    @Column(nullable = false)
    @Enumerated(STRING)
    private TaskPriority priority;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "author", nullable = false)
    private Person author;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "performer")
    private Person performer;
}

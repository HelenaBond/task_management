package com.bondareva.task_management.rest.model;

import com.bondareva.task_management.auth.model.Person;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.UuidGenerator;

import java.time.LocalDateTime;
import java.util.UUID;

import static jakarta.persistence.FetchType.LAZY;

@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Comment {

    @Id
    @GeneratedValue
    @UuidGenerator
    @Column(updatable = false)
    private UUID commentId;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "author", nullable = false)
    private Person author;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "task", nullable = false)
    private Task task;

    @Column(nullable = false)
    private String message;

    @Column(nullable = false)
    private LocalDateTime dateOfCreatingUtc;
}

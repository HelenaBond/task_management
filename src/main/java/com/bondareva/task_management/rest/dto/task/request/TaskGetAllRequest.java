package com.bondareva.task_management.rest.dto.task.request;

import com.bondareva.task_management.rest.dto.PageRequestDto;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Запрос на отфильтрованный, не упорядоченный, ограниченный в количестве список задач.")
public record TaskGetAllRequest(

        @Schema(description = "Образец фильтрации.", implementation = FilterTask.class)
        FilterTask filterDto,

        @Schema(description = "Пагинация. Номерация страниц начинается с 0.", implementation = PageRequestDto.class)
        PageRequestDto pageRequestDto
) {
}

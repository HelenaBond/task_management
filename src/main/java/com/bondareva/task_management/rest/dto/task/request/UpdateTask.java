package com.bondareva.task_management.rest.dto.task.request;

import com.bondareva.task_management.rest.enums.TaskPriority;
import com.bondareva.task_management.rest.enums.TaskStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Schema(description = """
        Запрос на обновление задачи.
         Все поля которые не должны быть изменены нужно заполнить старыми данными.
         Если нужно чтобы исполнитель не был назначен
         укажите "performerEmail" : "default@example.com".""")
public record UpdateTask(

        @Schema(description = "Название.")
        @Size(min = 2, max = 255, message = "Название задачи должно содержать от 2 до 255 символов.")
        @NotBlank(message = "Название задачи не может быть пустым.")
        String title,

        @Schema(description = "Описание.")
        @Size(min = 2, max = 2048, message = "Описание задачи должно содержать от 2 до 2048 символов.")
        @NotBlank(message = "Описание задачи не может быть пустым.")
        String description,

        @Schema(description = "Статус.", implementation = TaskStatus.class)
        @NotNull(message = "Статус не может быть пустым.")
        TaskStatus status,

        @Schema(description = "Приоритет.", implementation = TaskPriority.class)
        @NotNull(message = "Приоритет не может быть пустым.")
        TaskPriority priority,

        @Schema(description = "Почта автора.")
        @Size(min = 5, max = 255, message = "Адрес электронной почты автора должен содержать от 5 до 255 символов.")
        @NotBlank(message = "Адрес электронной почты автора не может быть пустым.")
        @Email(message = "Адрес электронной почты должен быть в формате user@example.com.")
        String authorEmail,

        @Schema(description = """
                Почта исполнителя. Если нужно чтобы исполнитель не был назначен
                 укажите "performerEmail" : "default@example.com".""")
        @Size(min = 5, max = 255, message = "Адрес электронной почты исполнителя должен содержать от 5 до 255 символов.")
        @NotBlank(message = """
                Адрес электронной почты исполнителя не может быть пустым.
                 Если нужно чтобы исполнитель не был назначен укажите default@example.com.""")
        @Email(message = "Адрес электронной почты должен быть в формате user@example.com.")
        String performerEmail
) {
}

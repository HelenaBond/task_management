package com.bondareva.task_management.rest.dto.task.request;

import com.bondareva.task_management.rest.enums.TaskPriority;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Schema(description = """
                         Запрос на создание новой задачи. Все поля обязательны.
                          Если исполнитель не назначен укажите "performerEmail" : "default@example.com".""")
public record CreateTask(

        @Schema(description = "Название.")
        @Size(min = 2, max = 255, message = "Название задачи должно содержать от 2 до 255 символов.")
        @NotBlank(message = "Название задачи не может быть пустым.")
        String title,

        @Schema(description = "Описание.")
        @Size(min = 2, max = 2048, message = "Описание задачи должно содержать от 2 до 2048 символов.")
        @NotBlank(message = "Описание задачи не может быть пустым.")
        String description,

        @Schema(description = "Приоритет.", implementation = TaskPriority.class)
        @NotNull(message = "Приоритет не может быть пустым.")
        TaskPriority priority,

        @Schema(description = "Исполнитель.")
        @Size(min = 5, max = 255, message = "Адрес электронной почты исполнителя должен содержать от 5 до 255 символов.")
        @NotBlank(message = """
                                Адрес электронной почты исполнителя не может быть пустым.
                                 Если исполнитель не назначен укажите "performerEmail" : "default@example.com".""")
        @Email(message = "Адрес электронной почты должен быть в формате user@example.com.")
        String performerEmail
) {
}

package com.bondareva.task_management.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;

@Schema(description = "Запрос на количество задач в списке и страницу пагинации. Номерация страниц начинается с 0.")
public record PageRequestDto(

        @Schema(description = "Запрашиваемая страница.")
        @Min(value = 0, message = "Нумерация страниц не может быть отрицательным числом. Номерация страниц начинается с 0.")
        int currentPage,

        @Schema(description = "Количество ожидаемых элементов в списке. После фильтрации их может быть меньше ожидаемого.")
        @Min(value = 10, message = "Количество запрашиваемых элементов не может быть меньше 10.")
        @Max(value = 100, message = "Количество запрашиваемых элементов не может быть больше 100.")
        @NotBlank(message = "Количество запрашиваемых элементов не может быть меньше 10.")
        int pageSize
) {
}

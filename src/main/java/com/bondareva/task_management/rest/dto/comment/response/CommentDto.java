package com.bondareva.task_management.rest.dto.comment.response;

import io.swagger.v3.oas.annotations.media.Schema;

import java.time.LocalDateTime;
import java.util.UUID;

@Schema(description = "Комментарий.")
public record CommentDto(

        @Schema(description = "Идентификатор.")
        UUID commentId,

        @Schema(description = "Почта автора коментария.")
        String authorEmail,

        @Schema(description = "Текст комментария.")
        String message,

        @Schema(description = "Дата создания комментария.")
        LocalDateTime dateOfCreatingUtc
) {
}

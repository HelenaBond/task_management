package com.bondareva.task_management.rest.dto.task.response;

import com.bondareva.task_management.rest.enums.TaskPriority;
import com.bondareva.task_management.rest.enums.TaskStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

@Schema(description = "Задача.")
public record TaskDto(

        @Schema(description = "Идентификатор.")
        UUID taskId,

        @Schema(description = "Название.")
        String title,

        @Schema(description = "Описание.")
        String description,

        @Schema(description = "Статус.", implementation = TaskStatus.class)
        @NotNull(message = "Статус не может быть пустым.")
        TaskStatus status,

        @Schema(description = "Приоритет.", implementation = TaskPriority.class)
        @NotNull(message = "Приоритет не может быть пустым.")
        TaskPriority priority,

        @Schema(description = "Почта автора.")
        String authorEmail,

        @Schema(description = "Почта исполнителя.")
        String performerEmail
) {
}

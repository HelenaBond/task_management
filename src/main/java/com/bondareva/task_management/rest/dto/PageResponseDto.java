package com.bondareva.task_management.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;

public record PageResponseDto(

        @Schema(description = """
                Сколько всего есть страниц с запрашиваемым количеством элементов.
                 Количество - это "pageSize" в pageRequestDto.""")
        int allPages,

        @Schema(description = "Запрашиваемая страница.")
        int currentPage
) {
}

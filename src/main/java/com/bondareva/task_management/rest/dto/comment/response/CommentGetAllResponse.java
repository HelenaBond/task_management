package com.bondareva.task_management.rest.dto.comment.response;

import com.bondareva.task_management.rest.dto.PageRequestDto;
import com.bondareva.task_management.rest.dto.PageResponseDto;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

@Schema(description = "Список коментариев к задаче.")
public record CommentGetAllResponse(

        @Schema(description = """
        Отфильтрованный, упорядоченный по дате добавления (сначала старые),
         ограниченный в количестве список коментариев к задаче.""")
        List<CommentDto> commentList,

        @Schema(description = "Пагинация.", implementation = PageRequestDto.class)
        PageResponseDto pageResponseDto
) {
}

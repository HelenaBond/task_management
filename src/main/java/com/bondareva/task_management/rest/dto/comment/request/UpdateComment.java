package com.bondareva.task_management.rest.dto.comment.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

@Schema(description = "Запрос на редактирование комментария.")
public record UpdateComment(

        @Schema(description = "Отредактированный коментарий.")
        @Size(max = 2048, message = "Комментарий может содержать до 2048 символов.")
        @NotBlank(message = "Комментарий не может быть пустым.")
        String message
) {
}

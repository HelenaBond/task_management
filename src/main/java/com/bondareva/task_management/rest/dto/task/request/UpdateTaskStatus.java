package com.bondareva.task_management.rest.dto.task.request;

import com.bondareva.task_management.rest.enums.TaskStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;

@Schema(description = "Запрос на обновление статуса задачи.")
public record UpdateTaskStatus(

        @Schema(description = "Статус задачи.", implementation = TaskStatus.class)
        @NotNull(message = "Статус не может быть пустым.")
        TaskStatus status
) {
}

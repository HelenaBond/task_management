package com.bondareva.task_management.rest.dto.task.response;

import com.bondareva.task_management.rest.dto.PageRequestDto;
import com.bondareva.task_management.rest.dto.PageResponseDto;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

@Schema(description = "Список задач.")
public record TaskGetAllResponse(

        @Schema(description = "Отфильтрованный, не упорядоченный, ограниченный в количестве список задач.",
                implementation = TaskDto.class)
        List<TaskDto> taskList,

        @Schema(description = "Пагинация.", implementation = PageRequestDto.class)
        PageResponseDto pageResponseDto
) {
}

package com.bondareva.task_management.rest.dto.task.request;

import com.bondareva.task_management.rest.enums.TaskPriority;
import com.bondareva.task_management.rest.enums.TaskStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;

@Schema(description = """
        Свойства по которым будет проведена фильтрация задач.
         Указывайте только те свойства по которым нужна фильтрация.
         Если вы хотите отфильтровать задачи у которых исполнитель не назначен,
         укажите "performerEmail" : "default@example.com".""")
public record FilterTask(

        @Schema(description = "Название.")
        @Size(min = 2, max = 255, message = "Название задачи должно содержать от 2 до 255 символов.")
        String title,

        @Schema(description = "Описание.")
        @Size(min = 2, max = 2048, message = "Описание задачи должно содержать от 2 до 2048 символов.")
        String description,

        @Schema(description = "Статус.", implementation = TaskStatus.class)
        TaskStatus status,

        @Schema(description = "Приоритет.", implementation = TaskPriority.class)
        TaskPriority priority,

        @Schema(description = "Почта автора.")
        @Size(min = 5, max = 255, message = "Адрес электронной почты автора должен содержать от 5 до 255 символов.")
        @Email(message = "Адрес электронной почты должен быть в формате user@example.com.")
        String authorEmail,

        @Schema(description = "Почта исполнителя.")
        @Size(min = 5, max = 255, message = "Адрес электронной почты исполнителя должен содержать от 5 до 255 символов.")
        @Email(message = """
                Адрес электронной почты должен быть в формате user@example.com.
                 Если вы хотите отфильтровать задачи у которых исполнитель не назначен,
                 укажите "performerEmail" : "default@example.com".""")
        String performerEmail
) {
}

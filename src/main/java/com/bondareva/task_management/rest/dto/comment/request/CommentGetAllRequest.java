package com.bondareva.task_management.rest.dto.comment.request;

import com.bondareva.task_management.rest.dto.PageRequestDto;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = """
        Запрос на отфильтрованный, упорядоченный по дате добавления (сначала старые),
         ограниченный в количестве список коментариев к задаче.""")
public record CommentGetAllRequest(

        @Schema(description = "Пагинация.", implementation = PageRequestDto.class)
        PageRequestDto pageRequestDto
) {
}

package com.bondareva.task_management.config;

import com.bondareva.task_management.auth.dto.UserDetailsImpl;
import com.bondareva.task_management.auth.repo.PersonRepo;
import com.bondareva.task_management.auth.service.JwtService;
import com.bondareva.task_management.exception.ErrorMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.UUID;

/**
 *  Сейчас этот класс валидирует только время жизни токена и email пользователя.
 * Это минимальная кастомная реализация без сохранения состояния.
 * Не безопасна в ситуации когда пользователю потребуется поменять пароль.
 * Для безопасного использования токена при общении с клиентом как минимум нужно сохранять сами токены и валидировать их.
 *  В Spring Security существует механизм аутентификации с использованием jwt по протоколу Auth2.
 * Для его использования понадобится настроить сервис ресурсов(часть с защищённым API),
 * сервис клиента(та часть которая отдаёт токены доступа к API) и сервис авторизации.
 */
@Component
@RequiredArgsConstructor
public class JwtAuthFilter extends OncePerRequestFilter {
    public static final String BEARER_PREFIX = "Bearer ";
    public static final String HEADER_NAME = "Authorization";
    private final Logger log = LogManager.getLogger();
    private final JwtService jwtService;
    private final PersonRepo personRepo;
    private final ObjectMapper objectMapper;

    @Override
    protected void doFilterInternal(
            @NonNull HttpServletRequest request,
            @NonNull HttpServletResponse response,
            @NonNull FilterChain filterChain
    ) throws ServletException, IOException {

        // Получаем токен из заголовка запроса
        String authHeader = request.getHeader(HEADER_NAME);
        if (StringUtils.isEmpty(authHeader) || !StringUtils.startsWith(authHeader, BEARER_PREFIX)) {
            filterChain.doFilter(request, response);
            return;
        }
        String jwt = authHeader.substring(BEARER_PREFIX.length());

        try {
            Claims claims = jwtService.extractAllClaims(jwt);
            String email = claims.getSubject();
            if (StringUtils.isNotEmpty(email) && personRepo.existsByEmail(email)) {
                SecurityContext context = SecurityContextHolder.getContext();
                Authentication authToken = createAuth(claims);

                context.setAuthentication(authToken);
                SecurityContextHolder.setContext(context);
            }
        } catch (ExpiredJwtException exception) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            ErrorMessage answer = new ErrorMessage("Время жизни токена истекло. Пожалуйста авторизуйтесь.");
            sendResponse(answer, response);
            return;
        } catch (JwtException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            ErrorMessage answer = new ErrorMessage("Невалидный токен.");
            log.error(answer.getMessage());
            sendResponse(answer, response);
            return;
        }
        filterChain.doFilter(request, response);
    }

    private void sendResponse(ErrorMessage message, HttpServletResponse response) throws IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(objectMapper.writeValueAsString(message));
    }

    private Authentication createAuth(Claims claims) {
        UUID personId = UUID.fromString(claims.get("personId", String.class));
        UserDetailsImpl principal = new UserDetailsImpl(personId, claims.getSubject(), null);

        // использование этого конструктора обязательно, тогда setAuthenticated(true)
        return new UsernamePasswordAuthenticationToken(
                principal,
                null,
                null
        );
    }
}

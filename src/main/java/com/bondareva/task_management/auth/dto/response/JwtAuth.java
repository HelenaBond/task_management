package com.bondareva.task_management.auth.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Ответ c токеном доступа.")
public record JwtAuth(

        @Schema(description = "Токен доступав котором есть email и id пользователя.")
        String token
) {
}

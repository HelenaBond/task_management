package com.bondareva.task_management.auth.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;

@Schema(description = "Запрос на регистрацию. Все поля обязательны.")
public record SignUp(

        @Schema(description = "Адрес электронной почты.", example = "user@example.com.")
        @Size(min = 5, max = 255, message = "Адрес электронной почты должен содержать от 5 до 255 символов.")
        @NotBlank(message = "Адрес электронной почты не может быть пустым.")
        @Email(message = "Адрес электронной почты должен быть в формате user@example.com.")
        String email,

        @Schema(description = "Пароль.")
        @Size(min = 8, max = 255, message = "Длина пароля должна быть от 8 до 255 символов.")
        @NotBlank(message = "Пароль не может быть пустым.")
        String password
) {
}

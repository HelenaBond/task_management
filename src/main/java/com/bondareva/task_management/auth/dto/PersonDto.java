package com.bondareva.task_management.auth.dto;

import java.util.UUID;

public record PersonDto(
        UUID personId,
        String email
) {
}

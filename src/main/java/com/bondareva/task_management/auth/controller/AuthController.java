package com.bondareva.task_management.auth.controller;

import com.bondareva.task_management.auth.dto.request.SignIn;
import com.bondareva.task_management.auth.dto.request.SignUp;
import com.bondareva.task_management.auth.dto.response.JwtAuth;
import com.bondareva.task_management.auth.service.AuthorizationService;
import com.bondareva.task_management.auth.service.RegistrationService;
import com.bondareva.task_management.exception.ErrorMessage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
@Tag(name = "Аутентификация")
@ApiResponses(value = {
        @ApiResponse(responseCode = "500", description = "Ошибка сервера. Обратитесь в техподдержку.",
                content = {@Content(mediaType = "application/json",
                        schema = @Schema(implementation = ErrorMessage.class))}),

        @ApiResponse(
                responseCode = "400",
                description = "Неверный формат данных запроса или отсутствуют обязательные поля.",
                content = {@Content(mediaType = "application/json",
                        schema = @Schema(implementation = ErrorMessage.class))})
})
public class AuthController {
    private final RegistrationService registrationService;
    private final AuthorizationService authorizationService;

    @Operation(summary = "Регистрация пользователя.")
    @ApiResponse(
            responseCode = "200",
            description = "Пользователь успешно зарегистрирован. Возвращается токен аутентификации.",
            content = @Content(mediaType = "application/json"))
    @ApiResponse(
            responseCode = "409",
            description = "Пользователь с такой электронной почтой уже существует.",

            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorMessage.class))})
    @PostMapping("/sign-up")

    public JwtAuth signUp(@RequestBody @Validated SignUp request) {
        return registrationService.signUp(request);
    }

    @Operation(summary = "Авторизация пользователя")
    @ApiResponse(
            responseCode = "200",
            description = "Авторизация успешна. Возвращается токен аутентификации.",
            content = @Content(mediaType = "application/json"))
    @ApiResponse(
            responseCode = "400",
            description = "Неверный запрос. Некорректные данные для входа.",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorMessage.class))})
    @ApiResponse(
            responseCode = "401",
            description = "Авторизация не удалась. Неверные учетные данные.",

            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorMessage.class))})
    @PostMapping("/sign-in")

    public JwtAuth signIn(@RequestBody @Validated SignIn request) {
        return authorizationService.signIn(request);
    }
}

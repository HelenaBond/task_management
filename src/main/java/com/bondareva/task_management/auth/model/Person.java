package com.bondareva.task_management.auth.model;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.UuidGenerator;

import java.util.UUID;

@Entity
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Person {

    @Id
    @GeneratedValue
    @UuidGenerator
    @Column(updatable = false)
    private UUID personId;

    @Column(unique = true)
    private String email;

    @Column(nullable = false)
    private String cryptoPassword;
}

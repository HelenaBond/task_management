package com.bondareva.task_management.auth.service;

import com.bondareva.task_management.auth.dto.UserDetailsImpl;
import com.bondareva.task_management.auth.model.Person;
import com.bondareva.task_management.auth.repo.PersonRepo;
import com.bondareva.task_management.exception.PersonNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final PersonRepo personRepo;

    /**
     * Поиск пользователя в базе для авторизации.
     *
     * @param email           почта
     *
     * @return данные
     */
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Person person = personRepo.findByEmail(email)
                .orElseThrow(() -> new PersonNotFoundException("Пользователь с почтой " + email + " не найден."));
        return new UserDetailsImpl(person.getPersonId(), person.getEmail(), person.getCryptoPassword());
    }
}

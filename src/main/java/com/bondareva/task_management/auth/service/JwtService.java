package com.bondareva.task_management.auth.service;

import com.bondareva.task_management.auth.dto.PersonDto;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class JwtService {

    @Value("${token.signing.key}")
    private String jwtSigningKey;

    public String generateToken(PersonDto person) {
        Map<String, Object> extraClaims = new HashMap<>();
        extraClaims.put("personId", person.personId());
        return generate(extraClaims, person);
    }

    /**
     * Генерация токена
     *
     * @param extraClaims дополнительные данные
     * @return токен
     */
    private String generate(Map<String, Object> extraClaims, PersonDto person) {
        return Jwts.builder()
                .claims()
                .subject(person.email())
                .add(extraClaims)
                .issuedAt(new Date(System.currentTimeMillis()))
                .expiration(new Date(System.currentTimeMillis() + 1000 * 60 * 30))
                .and()
                .signWith(getSigningKey())
                .compact();
    }

    private SecretKey getSigningKey() {
        byte[] keyBytes = Decoders.BASE64.decode(jwtSigningKey);
        return Keys.hmacShaKeyFor(keyBytes);
    }

    /**
     * Извлечение всех данных из токена.
     *
     * @param token           токен
     *      Автоматическая проверка на просроченность.
     *      Если токен просрочен будет выброшено исключение библиотекой jjwt
     *
     * @return данные
     */
    public Claims extractAllClaims(String token) {
        return Jwts.parser()
                .verifyWith(getSigningKey())
                .build()
                .parseSignedClaims(token)
                .getPayload();
    }
}

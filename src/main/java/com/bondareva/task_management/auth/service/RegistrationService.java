package com.bondareva.task_management.auth.service;

import com.bondareva.task_management.auth.dto.PersonDto;
import com.bondareva.task_management.auth.dto.request.SignUp;
import com.bondareva.task_management.auth.dto.response.JwtAuth;
import com.bondareva.task_management.auth.mapper.PersonMapper;
import com.bondareva.task_management.auth.model.Person;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RegistrationService {

    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final PersonService personService;
    private final PersonMapper personMapper;

    /**
     * Регистрация пользователя
     *
     * @param request данные пользователя
     * @return токен
     */
    public JwtAuth signUp(SignUp request) {
        String cryptoPass = passwordEncoder.encode(request.password());
        String email = request.email();
        Person person = personMapper.toPerson(request, cryptoPass);
        Person savedPerson = personService.save(person);
        PersonDto personDto = new PersonDto(savedPerson.getPersonId(), email);
        return new JwtAuth(jwtService.generateToken(personDto));
    }
}

package com.bondareva.task_management.auth.service;

import com.bondareva.task_management.auth.dto.PersonDto;
import com.bondareva.task_management.auth.dto.UserDetailsImpl;
import com.bondareva.task_management.auth.dto.request.SignIn;
import com.bondareva.task_management.auth.dto.response.JwtAuth;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthorizationService {

    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;

    /**
     * Авторизация пользователя
     *
     * @param request данные пользователя
     * @return токен
     */
    public JwtAuth signIn(SignIn request) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.email(), request.password())
        );
        UserDetailsImpl principal = (UserDetailsImpl) authentication.getPrincipal();
        PersonDto personDto = new PersonDto(principal.getId(), principal.getUsername());
        return new JwtAuth(jwtService.generateToken(personDto));
    }
}

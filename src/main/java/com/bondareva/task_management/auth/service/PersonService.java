package com.bondareva.task_management.auth.service;

import com.bondareva.task_management.auth.model.Person;
import com.bondareva.task_management.auth.repo.PersonRepo;
import com.bondareva.task_management.exception.PersonAlreadyExistsException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PersonService {

    private final PersonRepo personRepo;

    public Person save(Person person) {
        String email = person.getEmail();
        if (personRepo.existsByEmail(email)) {
            throw new PersonAlreadyExistsException("Пользователь с таким email уже существует.");
        }
        return personRepo.save(person);
    }
}

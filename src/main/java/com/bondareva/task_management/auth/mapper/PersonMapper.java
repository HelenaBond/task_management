package com.bondareva.task_management.auth.mapper;

import com.bondareva.task_management.auth.dto.request.SignUp;
import com.bondareva.task_management.auth.model.Person;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        injectionStrategy = InjectionStrategy.CONSTRUCTOR,
        unmappedSourcePolicy = ReportingPolicy.WARN)
public interface PersonMapper {

    @Mapping(target = "personId", ignore = true)
    @Mapping(target = "cryptoPassword", source = "cryptoPassword")
    Person toPerson(SignUp signUp, String cryptoPassword);
}

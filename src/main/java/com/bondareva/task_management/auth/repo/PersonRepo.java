package com.bondareva.task_management.auth.repo;

import com.bondareva.task_management.auth.dto.PersonDto;
import com.bondareva.task_management.auth.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface PersonRepo  extends JpaRepository<Person, UUID> {

    boolean existsByEmail(String email);

    Optional<Person> findByEmail(String email);

    Optional<PersonDto> findPersonDtoByEmail(String email);
}

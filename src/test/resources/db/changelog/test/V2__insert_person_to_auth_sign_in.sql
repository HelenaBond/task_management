--changeset bondareva:2023-05-04T16:55:00
INSERT INTO public.person (email, crypto_password)
 VALUES ('user2@example.com', '$2a$10$FX.p1PMrki/M1cJJOUkE9Oqiz9SDSc.gGbOMhEsUPQkPvJhpEchLO');
 INSERT INTO public.person (email, crypto_password)
  VALUES ('user3@example.com', '$2a$10$FX.p1PMrki/M1cJJOUkE9Oqiz9SDSc.gGbOMhEsUPQkPvJhpEchLO');

--changeset bondareva:2023-05-04T17:50:00
INSERT INTO public.person (email, crypto_password)
 VALUES ('user17@example.com', '$2a$10$FX.p1PMrki/M1cJJOUkE9Oqiz9SDSc.gGbOMhEsUPQkPvJhpEchLO');

INSERT INTO public.task (title, description, status, priority, author, performer)
 SELECT 'get all comments', 'description1', 'PROGRESS', 'HIGH', a.person_id, a.person_id
 FROM person a
 WHERE a.email = 'user17@example.com';

INSERT INTO public.comment (author, task, message, date_of_creating_utc)
 SELECT a.person_id, t.task_id, 'message6', '2023-05-03T07:45:00'
 FROM person a JOIN task t
 ON a.email = 'user17@example.com' AND t.title = 'get all comments';

INSERT INTO public.comment (author, task, message, date_of_creating_utc)
 SELECT a.person_id, t.task_id, 'message7', '2023-05-03T07:50:00'
 FROM person a JOIN task t
 ON a.email = 'user17@example.com' AND t.title = 'get all comments';
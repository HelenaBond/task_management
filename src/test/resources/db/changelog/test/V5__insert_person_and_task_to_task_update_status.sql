--changeset bondareva:2023-05-04T17:15:00
INSERT INTO public.person (email, crypto_password)
 VALUES ('user8@example.com', '$2a$10$FX.p1PMrki/M1cJJOUkE9Oqiz9SDSc.gGbOMhEsUPQkPvJhpEchLO');

INSERT INTO public.person (email, crypto_password)
 VALUES ('user9@example.com', '$2a$10$FX.p1PMrki/M1cJJOUkE9Oqiz9SDSc.gGbOMhEsUPQkPvJhpEchLO');

INSERT INTO public.task (title, description, status, priority, author, performer)
 SELECT 'update3', 'description1', 'PROGRESS', 'HIGH', a.person_id, p.person_id
 FROM person a, person p
 WHERE a.email = 'user8@example.com' and p.email = 'user9@example.com';

INSERT INTO public.task (title, description, status, priority, author, performer)
 SELECT 'update4', 'description2', 'PROGRESS', 'HIGH', a.person_id, a.person_id
 FROM person a
 WHERE a.email = 'user9@example.com';
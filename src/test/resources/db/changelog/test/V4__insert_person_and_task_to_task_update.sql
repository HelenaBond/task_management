--changeset bondareva:2023-05-04T17:10:00
INSERT INTO public.person (email, crypto_password)
 VALUES ('user6@example.com', '$2a$10$FX.p1PMrki/M1cJJOUkE9Oqiz9SDSc.gGbOMhEsUPQkPvJhpEchLO');

INSERT INTO public.person (email, crypto_password)
 VALUES ('user7@example.com', '$2a$10$FX.p1PMrki/M1cJJOUkE9Oqiz9SDSc.gGbOMhEsUPQkPvJhpEchLO');

INSERT INTO public.task (title, description, status, priority, author, performer)
 SELECT 'update1', 'description1', 'PROGRESS', 'HIGH', a.person_id, p.person_id
 FROM person a, person p
 WHERE a.email = 'user6@example.com' and p.email = 'user7@example.com';

INSERT INTO public.task (title, description, status, priority, author, performer)
 SELECT 'update2', 'description2', 'PROGRESS', 'HIGH', a.person_id, a.person_id
 FROM person a
 WHERE a.email = 'user7@example.com';
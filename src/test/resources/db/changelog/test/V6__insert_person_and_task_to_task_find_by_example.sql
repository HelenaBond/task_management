--changeset bondareva:2023-05-04T17:25:00
INSERT INTO public.person (email, crypto_password)
 VALUES ('user11@example.com', '$2a$10$FX.p1PMrki/M1cJJOUkE9Oqiz9SDSc.gGbOMhEsUPQkPvJhpEchLO');

INSERT INTO public.task (title, description, status, priority, author, performer)
 SELECT 'all task', 'description1', 'PROGRESS', 'HIGH', a.person_id, a.person_id
 FROM person a
 WHERE a.email = 'user11@example.com';

INSERT INTO public.task (title, description, status, priority, author, performer)
 SELECT 'All task', 'description1', 'PROGRESS', 'HIGH', a.person_id, a.person_id
 FROM person a
 WHERE a.email = 'user11@example.com';

INSERT INTO public.task (title, description, status, priority, author, performer)
 SELECT 'task', 'description1', 'PROGRESS', 'HIGH', a.person_id, a.person_id
 FROM person a
 WHERE a.email = 'user11@example.com';
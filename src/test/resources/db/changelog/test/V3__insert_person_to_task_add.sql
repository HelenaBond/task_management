--changeset bondareva:2023-05-04T17:05:00
INSERT INTO public.person (email, crypto_password)
 VALUES ('user4@example.com', '$2a$10$FX.p1PMrki/M1cJJOUkE9Oqiz9SDSc.gGbOMhEsUPQkPvJhpEchLO');
INSERT INTO public.person (email, crypto_password)
 VALUES ('user5@example.com', '$2a$10$FX.p1PMrki/M1cJJOUkE9Oqiz9SDSc.gGbOMhEsUPQkPvJhpEchLO');

--changeset bondareva:2023-05-04T16:50:00
INSERT INTO public.person (email, crypto_password)
 VALUES ('user1@example.com', '$2a$10$FX.p1PMrki/M1cJJOUkE9Oqiz9SDSc.gGbOMhEsUPQkPvJhpEchLO');
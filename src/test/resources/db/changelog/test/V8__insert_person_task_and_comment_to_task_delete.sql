--changeset bondareva:2023-05-04T17:40:00
INSERT INTO public.person (email, crypto_password)
 VALUES ('user13@example.com', '$2a$10$FX.p1PMrki/M1cJJOUkE9Oqiz9SDSc.gGbOMhEsUPQkPvJhpEchLO');

INSERT INTO public.task (title, description, status, priority, author, performer)
 SELECT 'with comment', 'description1', 'PROGRESS', 'HIGH', a.person_id, a.person_id
 FROM person a
 WHERE a.email = 'user13@example.com';

INSERT INTO public.comment (author, task, message, date_of_creating_utc)
 SELECT a.person_id, t.task_id, 'message1', '2023-05-03T07:45:00'
 FROM person a JOIN task t
 ON a.email = 'user13@example.com' AND t.title = 'with comment';

INSERT INTO public.comment (author, task, message, date_of_creating_utc)
 SELECT a.person_id, t.task_id, 'message2', '2023-05-03T07:50:00'
 FROM person a JOIN task t
 ON a.email = 'user13@example.com' AND t.title = 'with comment';


INSERT INTO public.person (email, crypto_password)
 VALUES ('user14@example.com', '$2a$10$FX.p1PMrki/M1cJJOUkE9Oqiz9SDSc.gGbOMhEsUPQkPvJhpEchLO');

INSERT INTO public.task (title, description, status, priority, author, performer)
 SELECT 'can not delete task', 'description1', 'PROGRESS', 'HIGH', a.person_id, a.person_id
 FROM person a
 WHERE a.email = 'user13@example.com';

INSERT INTO public.comment (author, task, message, date_of_creating_utc)
 SELECT a.person_id, t.task_id, 'message3', '2023-05-03T07:50:00'
 FROM person a JOIN task t
 ON a.email = 'user14@example.com' AND t.title = 'can not delete task';
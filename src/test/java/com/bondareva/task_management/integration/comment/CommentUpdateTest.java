package com.bondareva.task_management.integration.comment;

import com.bondareva.task_management.auth.controller.AuthController;
import com.bondareva.task_management.auth.dto.request.SignIn;
import com.bondareva.task_management.integration.AbstractRestControllerBaseTest;
import com.bondareva.task_management.rest.dto.comment.request.UpdateComment;
import com.bondareva.task_management.rest.repo.CommentRepo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.UUID;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("integration_test")
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class CommentUpdateTest  extends AbstractRestControllerBaseTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AuthController authController;

    @Autowired
    private CommentRepo commentRepo;

    @Test
    @DisplayName("Update comment successfully")
    public void givenUpdateCommentDto_whenUpdate_thenSuccessResponse() throws Exception {

        System.out.println("Given.");
        String jwt = authController.signIn(new SignIn("user18@example.com", "123456789")).token();
        UUID commentId = commentRepo.findByMessage("message8").getCommentId();
        UpdateComment updateComment = new UpdateComment("new message");

        System.out.println("When.");
        ResultActions result = mockMvc
                .perform(patch("/comment/" + commentId)
                        .header(AUTHORIZATION, "Bearer " + jwt)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updateComment)));

        System.out.println("Then.");
        result
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.commentId", CoreMatchers.is(commentId.toString())))
                .andExpect(jsonPath("$.authorEmail", CoreMatchers.is("user18@example.com")))
                .andExpect(jsonPath("$.message", CoreMatchers.is("new message")));
    }

    @Test
    @DisplayName("Can not update comment. Comment not found.")
    public void givenUpdateCommentDto_whenUpdateComment_thenCommentNotFoundErrorResponse() throws Exception {

        System.out.println("Given.");
        String jwt = authController.signIn(new SignIn("user18@example.com", "123456789")).token();
        UUID commentId = UUID.randomUUID();
        UpdateComment updateComment = new UpdateComment("some message");

        System.out.println("When.");
        ResultActions result = mockMvc
                .perform(patch("/comment/" + commentId)
                        .header(AUTHORIZATION, "Bearer " + jwt)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updateComment)));

        System.out.println("Then.");
        result
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", CoreMatchers.is(
                        "Комментарий не найден.")));
    }

    @Test
    @DisplayName("Can not update comment. User are not author.")
    public void givenUpdateCommentDto_whenUpdateComment_thenUserNotAuthorErrorResponse() throws Exception {

        System.out.println("Given.");
        String jwt = authController.signIn(new SignIn("user19@example.com", "123456789")).token();
        UUID commentId = commentRepo.findByMessage("message8").getCommentId();
        UpdateComment updateComment = new UpdateComment("some message");

        System.out.println("When.");
        ResultActions result = mockMvc
                .perform(patch("/comment/" + commentId)
                        .header(AUTHORIZATION, "Bearer " + jwt)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updateComment)));

        System.out.println("Then.");
        result
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", CoreMatchers.is(
                        "Вы не можете изменить комментарий. Вы не автор этого комментария.")));
    }
}

package com.bondareva.task_management.integration.comment;

import com.bondareva.task_management.auth.controller.AuthController;
import com.bondareva.task_management.auth.dto.request.SignIn;
import com.bondareva.task_management.integration.AbstractRestControllerBaseTest;
import com.bondareva.task_management.rest.dto.comment.request.CreateComment;
import com.bondareva.task_management.rest.model.Task;
import com.bondareva.task_management.rest.repo.TaskRepo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.UUID;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("integration_test")
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class CommentAddTest extends AbstractRestControllerBaseTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AuthController authController;

    @Autowired
    private TaskRepo taskRepo;

    @Test
    @DisplayName("Create comment successfully")
    public void givenCreateCommentDto_whenAdd_thenSuccessResponse() throws Exception {

        System.out.println("Given.");
        String jwt = authController.signIn(new SignIn("user12@example.com", "123456789")).token();
        Task test = taskRepo.findByTitle("to comment");
        CreateComment createComment = new CreateComment("new message");
        String createTime = LocalDateTime.now(ZoneOffset.UTC).withNano(0).toString();

        System.out.println("When.");
        ResultActions result = mockMvc
                .perform(post("/comment/" + test.getTaskId())
                        .header(AUTHORIZATION, "Bearer " + jwt)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(createComment)));

        System.out.println("Then.");
        result
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.commentId", CoreMatchers.notNullValue()))
                .andExpect(jsonPath("$.authorEmail", CoreMatchers.is("user12@example.com")))
                .andExpect(jsonPath("$.message", CoreMatchers.is("new message")))
                .andExpect(jsonPath("$.dateOfCreatingUtc", CoreMatchers.is(createTime)));
    }

    @Test
    @DisplayName("Can not create comment. Task not found.")
    public void givenCreateCommentDto_whenAdd_thenTaskNotFoundErrorResponse() throws Exception {

        System.out.println("Given.");
        String jwt = authController.signIn(new SignIn("user12@example.com", "123456789")).token();
        UUID taskId = UUID.randomUUID();
        CreateComment createComment = new CreateComment("new message");

        System.out.println("When.");
        ResultActions result = mockMvc
                .perform(post("/comment/" + taskId)
                        .header(AUTHORIZATION, "Bearer " + jwt)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(createComment)));

        System.out.println("Then.");
        result
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", CoreMatchers.is(
                        "Вы не можете оставить коментарий к выбранной задаче. Задача не найдена.")));
    }
}

package com.bondareva.task_management.integration.comment;

import com.bondareva.task_management.auth.controller.AuthController;
import com.bondareva.task_management.auth.dto.request.SignIn;
import com.bondareva.task_management.integration.AbstractRestControllerBaseTest;
import com.bondareva.task_management.rest.dto.PageRequestDto;
import com.bondareva.task_management.rest.dto.comment.request.CommentGetAllRequest;
import com.bondareva.task_management.rest.repo.TaskRepo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.UUID;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("integration_test")
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class CommentGetAllTest extends AbstractRestControllerBaseTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AuthController authController;

    @Autowired
    private TaskRepo taskRepo;

    @Test
    @DisplayName("Get all comments by taskId successfully.")
    public void givenCommentListRequestDto_whenGetAll_thenSuccessResponse() throws Exception {

        System.out.println("Given.");
        String jwt = authController.signIn(new SignIn("user17@example.com", "123456789")).token();
        UUID taskId = taskRepo.findByTitle("get all comments").getTaskId();
        PageRequestDto pageRequestDto = new PageRequestDto(0, 10);
        CommentGetAllRequest commentGetAllRequest = new CommentGetAllRequest(pageRequestDto);

        System.out.println("When.");
        ResultActions result = mockMvc
                .perform(get("/comment/all/" + taskId)
                        .header(AUTHORIZATION, "Bearer " + jwt)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(commentGetAllRequest)));

        System.out.println("Then.");
        result
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.commentList", hasSize(2)))
                .andExpect(jsonPath("$.commentList[0].message", is("message6")));
    }
}

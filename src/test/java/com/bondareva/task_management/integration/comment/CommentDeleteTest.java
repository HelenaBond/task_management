package com.bondareva.task_management.integration.comment;

import com.bondareva.task_management.auth.controller.AuthController;
import com.bondareva.task_management.auth.dto.request.SignIn;
import com.bondareva.task_management.integration.AbstractRestControllerBaseTest;
import com.bondareva.task_management.rest.repo.CommentRepo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("integration_test")
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class CommentDeleteTest extends AbstractRestControllerBaseTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AuthController authController;

    @Autowired
    private CommentRepo commentRepo;

    @Test
    @DisplayName("Delete comment successfully.")
    public void givenUuidComment_whenDelete_thenSuccessResponse() throws Exception {

        System.out.println("Given.");
        String jwt = authController.signIn(new SignIn("user15@example.com", "123456789")).token();
        UUID commentId = commentRepo.findByMessage("message4").getCommentId();

        System.out.println("When.");
        ResultActions result = mockMvc
                .perform(delete("/comment/" + commentId)
                        .header(AUTHORIZATION, "Bearer " + jwt)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(commentId)));

        System.out.println("Then.");
        result
                .andDo(print())
                .andExpect(status().isNoContent());
        assertThat(commentRepo.findByMessage("message4")).isNull();
    }

    @Test
    @DisplayName("Can not delete comment. User are not author.")
    public void givenUuidComment_whenDeleteComment_thenUserNotAuthorErrorResponse() throws Exception {

        System.out.println("Given.");
        String jwt = authController.signIn(new SignIn("user16@example.com", "123456789")).token();
        UUID commentId = commentRepo.findByMessage("message5").getCommentId();

        System.out.println("When.");
        ResultActions result = mockMvc
                .perform(delete("/comment/" + commentId)
                        .header(AUTHORIZATION, "Bearer " + jwt)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(commentId)));

        System.out.println("Then.");
        result
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", CoreMatchers.is(
                        "Вы не можете удалить комментарий. Вы не автор этого комментария.")));
    }

    @Test
    @DisplayName("Can not delete comment. Comment not found.")
    public void givenRandomUuid_whenDeleteComment_thenCommentNotFoundErrorResponse() throws Exception {

        System.out.println("Given.");
        String jwt = authController.signIn(new SignIn("user15@example.com", "123456789")).token();
        UUID commentId = UUID.randomUUID();

        System.out.println("When.");
        ResultActions result = mockMvc
                .perform(delete("/comment/" + commentId)
                        .header(AUTHORIZATION, "Bearer " + jwt)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(commentId)));

        System.out.println("Then.");
        result
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", CoreMatchers.is(
                        "Комментарий не найден.")));
    }

}

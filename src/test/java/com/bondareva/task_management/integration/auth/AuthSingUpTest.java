package com.bondareva.task_management.integration.auth;

import com.bondareva.task_management.auth.dto.request.SignUp;
import com.bondareva.task_management.integration.AbstractRestControllerBaseTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("integration_test")
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class AuthSingUpTest extends AbstractRestControllerBaseTest {

        @Autowired
        private MockMvc mockMvc;

        @Autowired
        private ObjectMapper objectMapper;

        @Test
        @DisplayName("Sign up successfully.")
        public void givenSignUp_whenSignUp_thenSuccessResponse() throws Exception {
            //given
            SignUp user = new SignUp("user@example.com", "123456789");

            //when
            ResultActions result = mockMvc
                    .perform(post("/auth/sign-up")
                            .contentType(APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(user)));

            //then
            result
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.token", notNullValue()));
        }

        @Test
        @DisplayName("Can not sign up. User already exist.")
        public void givenSignUp_whenSignUp_thenErrorResponsePersonAlreadyExist() throws Exception {
            //given
            SignUp user = new SignUp("user1@example.com", "123456789");

            //when
            ResultActions result = mockMvc
                    .perform(post("/auth/sign-up")
                            .contentType(APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(user)));

            //then
            result
                    .andDo(print())
                    .andExpect(status().isConflict())
                    .andExpect(jsonPath("$.message", is("Пользователь с таким email уже существует.")));
        }
}

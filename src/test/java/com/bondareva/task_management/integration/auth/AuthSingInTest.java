package com.bondareva.task_management.integration.auth;

import com.bondareva.task_management.auth.dto.request.SignIn;
import com.bondareva.task_management.integration.AbstractRestControllerBaseTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("integration_test")
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class AuthSingInTest extends AbstractRestControllerBaseTest {

        @Autowired
        private MockMvc mockMvc;

        @Autowired
        private ObjectMapper objectMapper;

        @Test
        @DisplayName("Sign in successfully.")
        public void givenSignIn_whenSignIn_thenSuccessResponse() throws Exception {
            //given
            SignIn user = new SignIn("user2@example.com", "123456789");

            //when
            ResultActions result = mockMvc
                    .perform(post("/auth/sign-in")
                            .contentType(APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(user)));

            //then
            result
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.token", CoreMatchers.notNullValue()));
        }

        @Test
        @DisplayName("Can not sign in. User not found.")
        public void givenSignIn_whenSignIn_thenErrorResponsePersonNotFound() throws Exception {
            //given
            SignIn user = new SignIn("noUser@example.com", "123456789");

            //when
            ResultActions result = mockMvc
                    .perform(post("/auth/sign-in")
                            .contentType(APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(user)));

            //then
            result
                    .andDo(print())
                    .andExpect(status().isForbidden())
                    .andExpect(jsonPath("$.message",
                            is("Пользователь с почтой " + user.email() + " не найден.")));
        }
}

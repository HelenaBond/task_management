package com.bondareva.task_management.integration.task;

import com.bondareva.task_management.auth.controller.AuthController;
import com.bondareva.task_management.auth.dto.request.SignIn;
import com.bondareva.task_management.integration.AbstractRestControllerBaseTest;
import com.bondareva.task_management.rest.dto.task.response.TaskDto;
import com.bondareva.task_management.rest.enums.TaskPriority;
import com.bondareva.task_management.rest.enums.TaskStatus;
import com.bondareva.task_management.rest.mapper.TaskMapper;
import com.bondareva.task_management.rest.model.Task;
import com.bondareva.task_management.rest.repo.TaskRepo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("integration_test")
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class TaskUpdateTest extends AbstractRestControllerBaseTest {

        @Autowired
        private MockMvc mockMvc;

        @Autowired
        private ObjectMapper objectMapper;

        @Autowired
        private AuthController authController;

        @Autowired
        private TaskRepo taskRepo;

        @Autowired
        private TaskMapper taskMapper;

        @Test
        @DisplayName("Update task successfully.")
        public void givenTaskDto_whenUpdate_thenSuccessResponse() throws Exception {

            System.out.println("Given.");
            String jwt = authController.signIn(new SignIn("user6@example.com", "123456789")).token();
            Task task = taskRepo.findByTitle("update1");
            task.setTitle("newTitle");
            TaskDto updateTask = taskMapper.fromTaskToTaskDto(task, "user6@example.com", "user7@example.com");

            System.out.println("When.");
            ResultActions result = mockMvc
                    .perform(put("/task/" + task.getTaskId())
                            .header(AUTHORIZATION, "Bearer " + jwt)
                            .contentType(APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(updateTask)));

            System.out.println("Then.");
            result
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.taskId", is(task.getTaskId().toString())))
                    .andExpect(jsonPath("$.title", is("newTitle")));
        }

        @Test
        @DisplayName("Can not update because task not found.")
        public void givenTaskDto_whenUpdate_thenTaskNotFoundErrorResponse() throws Exception {

            System.out.println("Given.");
            String jwt = authController.signIn(new SignIn("user6@example.com", "123456789")).token();
            TaskDto notValidTask = new TaskDto(
                    UUID.randomUUID(),
                    "title", "description",
                    TaskStatus.COMPLETE, TaskPriority.LOW,
                    "user6@example.com", "user6@example.com");

            System.out.println("When.");
            ResultActions result = mockMvc
                    .perform(put("/task/" + UUID.randomUUID())
                            .header(AUTHORIZATION, "Bearer " + jwt)
                            .contentType(APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(notValidTask)));

            System.out.println("Then.");
            result
                    .andDo(print())
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.message", is("Задача не найдена.")));
        }

        @Test
        @DisplayName("Can not update because user is not author.")
        public void givenTaskDto_whenUpdate_thenUserIsNotAuthorErrorResponse() throws Exception {

            System.out.println("Given.");
            String jwt = authController.signIn(new SignIn("user6@example.com", "123456789")).token();
            Task task = taskRepo.findByTitle("update2");
            TaskDto updateTask = taskMapper.fromTaskToTaskDto(task, "user7@example.com", "user7@example.com");

            System.out.println("When.");
            ResultActions result = mockMvc
                    .perform(put("/task/" + task.getTaskId())
                            .header(AUTHORIZATION, "Bearer " + jwt)
                            .contentType(APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(updateTask)));

            System.out.println("Then.");
            result
                    .andDo(print())
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.message", is(
                            "Вы не можете изменить эту задачу. Вы не автор выбранной задачи.")));
        }
}

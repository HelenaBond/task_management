package com.bondareva.task_management.integration.task;

import com.bondareva.task_management.auth.controller.AuthController;
import com.bondareva.task_management.auth.dto.request.SignIn;
import com.bondareva.task_management.config.JwtAuthFilter;
import com.bondareva.task_management.integration.AbstractRestControllerBaseTest;
import com.bondareva.task_management.rest.dto.task.request.CreateTask;
import com.bondareva.task_management.rest.enums.TaskPriority;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("integration_test")
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class TaskAddTest extends AbstractRestControllerBaseTest {

        @Autowired
        private MockMvc mockMvc;

        @Autowired
        private ObjectMapper objectMapper;

        @Autowired
        private AuthController authController;

        @Test
        @DisplayName("Create task with default performer.")
        public void givenCreateTaskDtoWithNullPerformer_whenAdd_thenSuccessResponse() throws Exception {

            System.out.println("Given.");
            String jwt = authController.signIn(new SignIn("user4@example.com", "123456789")).token();
            CreateTask createTask = new CreateTask(
                    "title", "description", TaskPriority.LOW, "default@example.com");

            System.out.println("When.");
            ResultActions result = mockMvc
                    .perform(post("/task")
                            .header(AUTHORIZATION, "Bearer " + jwt)
                            .contentType(APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(createTask)));

            System.out.println("Then.");
            result
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.taskId", notNullValue()))
                    .andExpect(jsonPath("$.title", is("title")))
                    .andExpect(jsonPath("$.description", is("description")))
                    .andExpect(jsonPath("$.priority", is("LOW")))
                    .andExpect(jsonPath("$.performerEmail", is("default@example.com")))
                    .andExpect(jsonPath("$.authorEmail", is("user4@example.com")));
        }


        @Test
        @DisplayName("Create task when author is performer.")
        public void givenCreateTaskDtoAndAuthorIsPerformer_whenAdd_thenSuccessResponse() throws Exception {

            System.out.println("Given.");
            String jwt = authController.signIn(new SignIn("user4@example.com", "123456789")).token();
            CreateTask createTask = new CreateTask(
                    "title", "description", TaskPriority.LOW, "user4@example.com");

            System.out.println("When.");
            ResultActions result = mockMvc
                    .perform(post("/task")
                            .header(AUTHORIZATION, "Bearer " + jwt)
                            .contentType(APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(createTask)));

            System.out.println("Then.");
            result
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.taskId", notNullValue()))
                    .andExpect(jsonPath("$.title", is("title")))
                    .andExpect(jsonPath("$.description", is("description")))
                    .andExpect(jsonPath("$.priority", is("LOW")))
                    .andExpect(jsonPath("$.performerEmail", is("user4@example.com")))
                    .andExpect(jsonPath("$.authorEmail", is("user4@example.com")));
        }

        @Test
        @DisplayName("Create task when performer is not author.")
        public void givenCreateTaskDtoAndPerformerIsNotAuthor_whenAdd_thenSuccessResponse() throws Exception {

            System.out.println("Given.");
            String jwt = authController.signIn(new SignIn("user4@example.com", "123456789")).token();
            CreateTask createTask = new CreateTask(
                    "title", "description", TaskPriority.LOW, "user5@example.com");

            System.out.println("When.");
            ResultActions result = mockMvc
                    .perform(post("/task")
                            .header(AUTHORIZATION, "Bearer " + jwt)
                            .contentType(APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(createTask)));

            System.out.println("Then.");
            result
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.taskId", notNullValue()))
                    .andExpect(jsonPath("$.title", is("title")))
                    .andExpect(jsonPath("$.description", is("description")))
                    .andExpect(jsonPath("$.priority", is("LOW")))
                    .andExpect(jsonPath("$.authorEmail", is("user4@example.com")))
                    .andExpect(jsonPath("$.performerEmail", is("user5@example.com")));
        }

        @Test
        @DisplayName("Can not create task when jwt not valid.")
        public void givenCreateTaskDtoAndNotValidToken_whenAdd_thenErrorResponse() throws Exception {

            System.out.println("Given.");
            String jwt = JwtAuthFilter.BEARER_PREFIX + "not valid token.";
            CreateTask createTask = new CreateTask(
                    "title", "description", TaskPriority.LOW, "user5@example.com");

            System.out.println("When.");
            ResultActions result = mockMvc
                    .perform(post("/task")
                            .header(AUTHORIZATION, "Bearer " + jwt)
                            .contentType(APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(createTask)));

            System.out.println("Then.");
            result
                    .andDo(print())
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.message", CoreMatchers.is("Невалидный токен.")));
        }
}

package com.bondareva.task_management.integration.task;

import com.bondareva.task_management.auth.controller.AuthController;
import com.bondareva.task_management.auth.dto.request.SignIn;
import com.bondareva.task_management.integration.AbstractRestControllerBaseTest;
import com.bondareva.task_management.rest.model.Task;
import com.bondareva.task_management.rest.repo.CommentRepo;
import com.bondareva.task_management.rest.repo.TaskRepo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("integration_test")
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class TaskDeleteTest extends AbstractRestControllerBaseTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AuthController authController;

    @Autowired
    private TaskRepo taskRepo;

    @Autowired
    private CommentRepo commentRepo;


    @Test
    @DisplayName("Delete task successfully.")
    public void givenTaskUUID_whenDelete_thenSuccessResponse() throws Exception {

        System.out.println("Given.");
        String jwt = authController.signIn(new SignIn("user13@example.com", "123456789")).token();
        Task task = taskRepo.findByTitle("with comment");
        UUID taskId = task.getTaskId();

        System.out.println("When.");
        ResultActions result = mockMvc
                .perform(delete("/task/" + taskId)
                        .header(AUTHORIZATION, "Bearer " + jwt)
                        .contentType(APPLICATION_JSON));

        System.out.println("Then.");
        result
                .andDo(print())
                .andExpect(status().isNoContent());
        assertThat(taskRepo.findByTitle("with comment")).isNull();
        assertThat(commentRepo.findByMessage("message1")).isNull();
        assertThat(commentRepo.findByMessage("message2")).isNull();
    }

    @Test
    @DisplayName("Can not delete task. Task not found.")
    public void givenNoExistTaskUUID_whenDeleteTask_thenTaskNotFoundErrorResponse() throws Exception {

        System.out.println("Given.");
        String jwt = authController.signIn(new SignIn("user13@example.com", "123456789")).token();
        UUID taskId = UUID.randomUUID();

        System.out.println("When.");
        ResultActions result = mockMvc
                .perform(delete("/task/" + taskId)
                        .header(AUTHORIZATION, "Bearer " + jwt)
                        .contentType(APPLICATION_JSON));

        System.out.println("Then.");
        result
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", CoreMatchers.is("Задача не найдена.")));
    }

    @Test
    @DisplayName("Can not delete task. User not author.")
    public void givenTaskId_whenDeleteTask_thenUserNotAuthorErrorResponse() throws Exception {

        System.out.println("Given.");
        String jwt = authController.signIn(new SignIn("user14@example.com", "123456789")).token();
        Task task = taskRepo.findByTitle("can not delete task");
        UUID taskId = task.getTaskId();

        System.out.println("When.");
        ResultActions result = mockMvc
                .perform(delete("/task/" + taskId)
                        .header(AUTHORIZATION, "Bearer " + jwt)
                        .contentType(APPLICATION_JSON));

        System.out.println("Then.");
        result
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", CoreMatchers.is(
                        "Вы не можете удалить эту задачу. Вы не автор выбранной задачи.")));


    }
}

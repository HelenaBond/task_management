package com.bondareva.task_management.integration.task;

import com.bondareva.task_management.auth.controller.AuthController;
import com.bondareva.task_management.auth.dto.request.SignIn;
import com.bondareva.task_management.integration.AbstractRestControllerBaseTest;
import com.bondareva.task_management.rest.dto.task.request.UpdateTaskStatus;
import com.bondareva.task_management.rest.enums.TaskStatus;
import com.bondareva.task_management.rest.model.Task;
import com.bondareva.task_management.rest.repo.TaskRepo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("integration_test")
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class TaskUpdateStatusTest extends AbstractRestControllerBaseTest {


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AuthController authController;

    @Autowired
    private TaskRepo taskRepo;

    @Test
    @DisplayName("Update status task successfully.")
    public void givenTaskDto_whenPerformerUpdateStatus_thenSuccessResponse() throws Exception {

        System.out.println("Given.");
        String jwt = authController.signIn(new SignIn("user8@example.com", "123456789")).token();
        UUID taskId = taskRepo.findByTitle("update3").getTaskId();
        UpdateTaskStatus updateTask = new UpdateTaskStatus(TaskStatus.COMPLETE);

        System.out.println("When.");
        ResultActions result = mockMvc
                .perform(patch("/task/" + taskId + "/status")
                        .header(AUTHORIZATION, "Bearer " + jwt)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updateTask)));

        System.out.println("Then.");
        result
                .andDo(print())
                .andExpect(status().isAccepted());
        Task updatedTask = taskRepo.findByTitle("update3");
        assertThat(updatedTask.getStatus()).isEqualTo(TaskStatus.COMPLETE);
    }

    @Test
    @DisplayName("Can not update status task.")
    public void givenTaskDto_whenUpdateStatus_thenUserIsNotAuthorOrPerformerErrorResponse() throws Exception {

        System.out.println("Given.");
        String jwt = authController.signIn(new SignIn("user8@example.com", "123456789")).token();
        UUID taskId = taskRepo.findByTitle("update4").getTaskId();
        UpdateTaskStatus updateTask = new UpdateTaskStatus(TaskStatus.COMPLETE);

        System.out.println("When.");
        ResultActions result = mockMvc
                .perform(patch("/task/" + taskId + "/status")
                        .header(AUTHORIZATION, "Bearer " + jwt)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updateTask)));

        System.out.println("Then.");
        result
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is(
                        "Вы не можете изменить статус этой задачи. Вы не исполнитель и не автор выбранной задачи.")));
    }
}

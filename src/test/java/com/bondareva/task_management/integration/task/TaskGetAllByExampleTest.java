package com.bondareva.task_management.integration.task;

import com.bondareva.task_management.auth.controller.AuthController;
import com.bondareva.task_management.auth.dto.request.SignIn;
import com.bondareva.task_management.integration.AbstractRestControllerBaseTest;
import com.bondareva.task_management.rest.dto.PageRequestDto;
import com.bondareva.task_management.rest.dto.task.request.FilterTask;
import com.bondareva.task_management.rest.dto.task.request.TaskGetAllRequest;
import com.bondareva.task_management.rest.enums.TaskStatus;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ActiveProfiles("integration_test")
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class TaskGetAllByExampleTest extends AbstractRestControllerBaseTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AuthController authController;

    @Test
    @DisplayName("Find all by performer, status, and part of title.")
    public void givenExampleWithPartOfTitleStatusPerformer_whenFindAll_thenSuccessResponse() throws Exception {

        System.out.println("Given.");
        String jwt = authController.signIn(new SignIn("user11@example.com", "123456789")).token();
        FilterTask filter= new FilterTask(
                "all", null, TaskStatus.PROGRESS, null, null, "user11@example.com");
        PageRequestDto page = new PageRequestDto(0, 10);

        System.out.println("When.");
        ResultActions result = mockMvc
                .perform(get("/task/all")
                .header(AUTHORIZATION, "Bearer " + jwt)
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new TaskGetAllRequest(filter, page))));

        System.out.println("Then.");
        result
                .andDo(print())
                .andExpect(jsonPath("$.taskList", hasSize(2)));
    }
}
